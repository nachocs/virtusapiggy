import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { Splash } from '../pages/splash/splash';
import { Dashboard } from '../pages/dashboard/dashboard';
import { Earn } from '../pages/earn/earn';
import { Signup } from '../pages/signup/signup';
import { Page3 } from '../pages/page3/page3';
import { Page4 } from '../pages/page4/page4';
import { SummaryScreen } from '../pages/summary-screen/summary-screen';
import { ToneSelector } from '../pages/tone-selector/tone-selector';
import { KeySpending1 } from '../pages/key-spending1/key-spending1';
import { SaveMore } from '../pages/save-more/save-more';
import { LearnMore } from '../pages/learn-more/learn-more';
import { VoucherVault } from '../pages/voucher-vault/voucher-vault';
import { LinkedAccounts } from '../pages/linked-accounts/linked-accounts';
import { GoalsAccounts } from '../pages/goals-accounts/goals-accounts';
import { NewGoal } from '../pages/new-goal/new-goal';
import { Goals } from '../pages/goals/goals';
import { GoalBreakdown } from '../pages/goal-breakdown/goal-breakdown';
import { Budget } from '../pages/budget/budget';
import { SocialChannel } from '../pages/social-channel/social-channel';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Splash;

  pages: Array<{title: string, component: any, icon: any}>;
  pages2: Array<{title: string, component: any, icon: any}>;

  constructor(public platform: Platform, public menuCtrl: MenuController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Spend', component: KeySpending1, icon: 'assets/images/banknote-icon.png'},
      { title: 'Goals', component: Goals, icon: 'assets/images/goals-icon.png' },
      { title: 'Budgeting', component: Budget, icon: 'assets/images/coins-icon.png' },
      { title: 'Earn', component: Earn, icon: 'assets/images/earn-icon.png' },
      { title: 'Learn', component: LearnMore, icon: 'assets/images/learn-icon.png'},
      { title: 'Save', component: SaveMore, icon: 'assets/images/save-icon.png'},
    ];
    this.pages2 = [
      { title: 'Preferences', component: Dashboard, icon: 'assets/images/settings-256.png'},
      { title: 'Accounts', component: LinkedAccounts, icon: null },
      { title: 'Pigby', component: ToneSelector, icon: null },
      { title: 'Social channel', component: SocialChannel, icon: null },
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      // Splashscreen.hide();
    });
  }
  ngAfterViewInit(){
    this.menuCtrl.enable(false);
  }
  openPage(page) {
    this.nav.push(page.component);
  }
}
