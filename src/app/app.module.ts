import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';
import { Splash } from '../pages/splash/splash';
import { Signup } from '../pages/signup/signup';
import { Page3 } from '../pages/page3/page3';
import { Page4 } from '../pages/page4/page4';
import { SummaryScreen } from '../pages/summary-screen/summary-screen';
import { ToneSelector } from '../pages/tone-selector/tone-selector';
import { Dashboard } from '../pages/dashboard/dashboard';
import { Earn } from '../pages/earn/earn';
import { Linkedin } from '../pages/linkedin/linkedin';
import { Linkedin2 } from '../pages/linkedin2/linkedin2';
import { Linkedin3 } from '../pages/linkedin3/linkedin3';
import { KeySpending1 } from '../pages/key-spending1/key-spending1';
import { SaveMore } from '../pages/save-more/save-more';
import { LearnMore } from '../pages/learn-more/learn-more';
import { VoucherVault } from '../pages/voucher-vault/voucher-vault';
import { LinkedAccounts } from '../pages/linked-accounts/linked-accounts';
import { GoalsAccounts } from '../pages/goals-accounts/goals-accounts';
import { NewGoal } from '../pages/new-goal/new-goal';
import { Goals } from '../pages/goals/goals';
import { GoalBreakdown } from '../pages/goal-breakdown/goal-breakdown';
import { Budget } from '../pages/budget/budget';
import { Budget2 } from '../pages/budget2/budget2';
import { SocialChannel } from '../pages/social-channel/social-channel';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    MyApp,
    Splash,
    Signup,
    Page3,
    Page4,
    SummaryScreen,
    ToneSelector,
    Dashboard,
    Earn,
    Linkedin,
    Linkedin2,
    Linkedin3,
    KeySpending1,
    SaveMore,
    LearnMore,
    VoucherVault,
    LinkedAccounts,
    GoalsAccounts,
    KeySpending1,
    NewGoal,
    Goals,
    GoalBreakdown,
    Budget,
    Budget2,
    SocialChannel
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    FormsModule,
    ChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Splash,
    Signup,
    Page3,
    Page4,
    SummaryScreen,
    ToneSelector,
    Dashboard,
    Earn,
    Linkedin,
    Linkedin2,
    Linkedin3,
    KeySpending1,
    SaveMore,
    LearnMore,
    VoucherVault,
    LinkedAccounts,
    GoalsAccounts,
    KeySpending1,
    NewGoal,
    Goals,
    GoalBreakdown,
    Budget,
    Budget2,
    SocialChannel
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
