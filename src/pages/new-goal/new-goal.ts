import { Component,NgModule } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BrowserModule  } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Goals } from '../goals/goals';


@Component({
  selector: 'page-new-goal',
  templateUrl: 'new-goal.html'
})

@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [ Component ],
    bootstrap:    [ Component ]
})

export class NewGoal {

	goals= Goals;
	
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewGoal');
  }

  saveGoal(){
  	console.log('[New Goal] --------- saved');
  }
}
