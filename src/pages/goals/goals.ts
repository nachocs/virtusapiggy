import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NewGoal } from '../new-goal/new-goal';
import { GoalBreakdown } from '../goal-breakdown/goal-breakdown';
import { Dashboard } from '../dashboard/dashboard';

/*
  Generated class for the Goals page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-goals',
  templateUrl: 'goals.html'
})
export class Goals {

	newGoal = NewGoal;
	breakdown = GoalBreakdown;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoalsPage');
  }
  goBack(){
    this.navCtrl.pop();
  }
}
