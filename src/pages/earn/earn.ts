import { Component, ViewChild } from '@angular/core';

import { NavController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { Linkedin } from '../linkedin/linkedin';
import { Dashboard } from '../dashboard/dashboard';
@Component({
  selector: 'page-earn',
  templateUrl: 'earn.html'
})
export class Earn {
  @ViewChild(Slides) slides: Slides;
  // currentIndex: Number;

  constructor(public navCtrl: NavController) {

  }
  ngOnInit(){
  }
  slideChanged() {
    // this.currentIndex = this.slides.getActiveIndex();
    // console.log("Current index is", this.currentIndex);
  }
  goBack(){
    this.navCtrl.pop();
  }
  goNext(){
    if (this.slides.getActiveIndex() === 0){
      this.navCtrl.push(Linkedin);
    }
  }
}
