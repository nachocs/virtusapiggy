import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Slides } from 'ionic-angular';
import { Signup } from '../signup/signup';

@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html'
})
export class Splash {
  @ViewChild(Slides) slides: Slides;
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  signup = Signup;

  ngOnInit(){
    this.slides.lockSwipes(false);
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log("Current index is", currentIndex);
  }
}
