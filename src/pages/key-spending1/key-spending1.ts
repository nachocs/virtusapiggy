import { Component, NgModule } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BrowserModule  } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Dashboard } from '../dashboard/dashboard';


@Component({
  selector: 'page-key-spending1',
  templateUrl: 'key-spending1.html'
})

@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [ Component ],
    bootstrap:    [ Component ]
})
export class KeySpending1 {

dashboard = Dashboard;

spendings: string = "vendors";
period: string = "today";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  
  ionViewDidLoad() {
    console.log('ionViewDidLoad KeySpending1Page');
  }


}
