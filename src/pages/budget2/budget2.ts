import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Budget } from '../budget/budget';
import Chart from 'chart.js';

@Component({
  selector: 'page-budget2',
  templateUrl: 'budget2.html'
})
export class Budget2 {
  // Doughnut
  pages: Array<{title: string, component: any, icon: any}>;

  public doughnutChartLabels:Array<any> = ['12.5% Travel cost', '50% Food', '12.5% Mobile phone', '25% Home bills'];
  public doughnutChartData:number[] = [12.5, 50, 12.5, 25];
  public doughnutChartType:string = 'doughnut';
  public doughnutOptions:any = {
    cutoutPercentage:80,
    responsive:true,
    legend:{
      labels:{
        fontColor:'#FFFFFF'
      }
    }
  };
  public doughnutColors:Array<any> = [{ backgroundColor: ["#00b2bc", "#baea78", "#ffffff", "#db2e7e"] , borderColor:'#401166', borderWidth:0}];


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Budget2Page');
  }
  goBack(){
    this.navCtrl.pop();
  }
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }
}
