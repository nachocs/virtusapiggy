import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'voucher-vault',
  templateUrl: 'voucher-vault.html'
})

export class VoucherVault {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  goBack(){
    this.navCtrl.pop();
  }
}
