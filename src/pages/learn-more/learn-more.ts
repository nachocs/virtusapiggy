import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { Dashboard } from '../dashboard/dashboard';

@Component({
  selector: 'learn-more',
  templateUrl: 'learn-more.html'
})

export class LearnMore {  

	dashboard = Dashboard;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }
}
