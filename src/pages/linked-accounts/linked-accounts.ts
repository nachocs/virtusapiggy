import { Component, ViewChild } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { GoalsAccounts } from '../goals-accounts/goals-accounts';
import { VoucherVault } from '../voucher-vault/voucher-vault';


@Component({
  selector: 'linked-accounts',
  templateUrl: 'linked-accounts.html'
})

export class LinkedAccounts {

    accountType: string;

    goalsAccounts = GoalsAccounts;
    voucherVault = VoucherVault;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        this.accountType = "bank-accounts";
    }

    goBack(){
        this.navCtrl.pop();
    }
}
