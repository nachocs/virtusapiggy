import { Component, ViewChild } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'goals-accounts',
  templateUrl: 'goals-accounts.html'
})

export class GoalsAccounts {

    constructor(public navCtrl: NavController, public navParams: NavParams) {

    }

    goBack(){
        this.navCtrl.pop();
    }
}
