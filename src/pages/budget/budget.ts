import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Dashboard } from '../dashboard/dashboard';
import { Budget2 } from '../budget2/budget2';

@Component({
  selector: 'page-budget',
  templateUrl: 'budget.html'
})
export class Budget {
  budget2 = Budget2;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad BudgetPage');
  }
  goBack(){
    this.navCtrl.pop();
  }
}
