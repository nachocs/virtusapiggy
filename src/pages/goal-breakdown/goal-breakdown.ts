import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Goals } from '../goals/goals';
import { Earn } from '../earn/earn';
import { SaveMore } from '../save-more/save-more';
import { LearnMore } from '../learn-more/learn-more';

/*
  Generated class for the GoalBreakdown page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-goal-breakdown',
  templateUrl: 'goal-breakdown.html'
})
export class GoalBreakdown {

goals = Goals;
earn = Earn;
learn = LearnMore;
save = SaveMore;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoalBreakdownPage');
  }

  showHideBlurb(){
  	var blurb = document.getElementsByClassName('blurb')[0];

  	if(blurb.classList.contains('hidden')){
  		blurb.classList.remove('hidden')
  	} else {
  		blurb.classList.add('hidden')
  	}
  }
}
