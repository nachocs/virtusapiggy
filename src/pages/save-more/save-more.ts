import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'save-more',
  templateUrl: 'save-more.html'
})

export class SaveMore {  
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  goBack(){
    this.navCtrl.pop();
  }

}
