import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { ToneSelector } from '../tone-selector/tone-selector';


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class Dashboard {
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {

  }
  ionViewDidLoad() {
    this.menuCtrl.enable(true).open();
  }
  ionViewWillEnter() { 
        console.log('ionViewWillEnter Dashboard -> open menu');
        return this.menuCtrl.enable(true).open();

    }
}
