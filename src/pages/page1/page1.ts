import { Component, ViewChild } from '@angular/core';

import { NavController } from 'ionic-angular';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController) {

  }
  ngOnInit(){
    // this.slides.lockSwipes(true);
  }
  goSlide(){
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipeToNext(true);
  }
  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log("Current index is", currentIndex);
  }
}
