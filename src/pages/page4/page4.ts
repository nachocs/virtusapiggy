import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { ToneSelector } from '../tone-selector/tone-selector';
/*
  Generated class for the Page4 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-page4',
  templateUrl: 'page4.html'
})
export class Page4 {
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad Page4Page');
  }
  goBack(){
    this.navCtrl.pop();
  }
  goNext(){
    this.navCtrl.push(ToneSelector);
  }
}
