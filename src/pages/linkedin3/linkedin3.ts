import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Slides } from 'ionic-angular';

@Component({
  selector: 'page-linkedin3',
  templateUrl: 'linkedin3.html'
})
export class Linkedin3 {
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Linkedin3');
  }
  goNext(){
    // this.navCtrl.push(Linkedin3);
  }
  goBack(){
    this.navCtrl.pop();
  }
}
