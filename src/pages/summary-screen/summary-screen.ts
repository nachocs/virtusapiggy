import { Component, ViewChild } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { Dashboard} from '../dashboard/dashboard';
@Component({
  selector: 'page-summary-screen',
  templateUrl: 'summary-screen.html'
})

export class SummaryScreen {

  dashboard = Dashboard;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  goBack(){
      this.navCtrl.pop();
  }
  goNext(){
    this.navCtrl.push(Dashboard);
  }
}
