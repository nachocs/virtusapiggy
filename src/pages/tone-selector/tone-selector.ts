import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { SummaryScreen } from '../summary-screen/summary-screen';

@Component({
  selector: 'tone-selector',
  templateUrl: 'tone-selector.html'
})

export class ToneSelector {
  
  tone: number;
  rotation: string;
  
  topStatus: string;
  leftStatus: string;
  rightStatus: string;

  summaryScreen = SummaryScreen;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.rotation = "";

    this.leftStatus = "inactive";
    this.rightStatus = "inactive";
    this.topStatus = "active";
  }

  setLowTone(tone) {

    this.tone = -90;
    this.rotation = "rotation low";

    this.leftStatus = "active";
    this.rightStatus = "inactive";
    this.topStatus = "inactive";
  }

  setMediumTone(){
    this.tone = 0;
    this.rotation = "rotation medium";

    this.leftStatus = "inactive";
    this.rightStatus = "inactive";
    this.topStatus = "active";
  }

  setHardTone(){
    this.tone = 90;
    this.rotation = "rotation hard";

    this.leftStatus = "inactive";
    this.rightStatus = "active";
    this.topStatus = "inactive";
  }

  goBack(){
    this.navCtrl.pop();
  }

  goNext(){
    this.navCtrl.push(SummaryScreen);
  }
}
