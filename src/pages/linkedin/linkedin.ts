import { Component, ViewChild } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { NavController, Platform } from 'ionic-angular';
import { Linkedin2 } from '../linkedin2/linkedin2';
// import {Facebook, Google, LinkedIn} from "ng2-cordova-oauth/core";
// import {OauthCordova} from 'ng2-cordova-oauth/platform/cordova';
// import {OauthBrowser} from 'ng2-cordova-oauth/platform/browser'

declare var IN: any;

@Component({
  selector: 'page-linkedin',
  templateUrl: 'linkedin.html'
})
export class Linkedin {
  // private oauth: OauthCordova = new OauthCordova();
  // private access_token: string;
  // private oauth: OauthBrowser = new OauthBrowser();
  // private redirectUri =  'http://localhost:8100';
  // private clientId = "776obow88axl48";
  // private clientSecret = "ZiAwHjgjYo2p1OUf";
  // private linkedInProvider: LinkedIn = new LinkedIn({
  //   clientId: this.clientId,
  //   appScope: ["r_basicprofile","r_emailaddress"],
  //   redirectUri: this.redirectUri,
  //   responseType: 'code',
  //   state:  'DCEeFWf45A53sdfKef424QQQ'
  // });
  linkedInAuthCheck:any;
  linkedInAuthCheckCounter:number = 0;
  agree: boolean;
  constructor(public navCtrl: NavController, public platform: Platform, public http: Http) {

  }
  ngOnInit(){
  }
  ngOnDestroy(){
    if (this.linkedInAuthCheck){
      clearInterval(this.linkedInAuthCheck);
    }
  }
  goBack(){
    if (this.linkedInAuthCheck){
      clearInterval(this.linkedInAuthCheck);
    }
    this.navCtrl.pop();
  }
  goNext(){
    var self = this;

    // Handle the successful return from the API call
    function onSuccess(data){
      console.log(data);
      if (self.linkedInAuthCheck){
        clearInterval(self.linkedInAuthCheck);
      }
      self.navCtrl.push(Linkedin2, data);
    }

    // Handle an error response from the API call
    function onError(error){
      console.log(error);
    }
    // Use the API call wrapper to request the member's basic profile data
    function getProfileData(){
      IN.API.Raw("/people/~:(id,picture-url,firstName,lastName,headline,public-profile-url,formatted-name,positions)").result(onSuccess).error(onError);
    }
    IN.Event.on(IN, "auth", function (){
      console.log('auth');
      getProfileData();
    }, this);
    if (this.agree){
      if (!IN.User.isAuthorized()){
        IN.User.authorize(getProfileData, this);
        this.linkedInAuthCheck = setInterval(()=>{
          this.linkedInAuthCheckCounter++;
          if (this.linkedInAuthCheckCounter > 15){
            clearInterval(this.linkedInAuthCheck);
          }
          if (IN.User.isAuthorized()){
            getProfileData();
          }
        }, 3000);
      } else {
        getProfileData();
      }
    }
  }
  // goNextMobile(){
  //   if (this.agree){
  //     this.oauth.logInVia(this.linkedInProvider).then((success:any) => {
  //       let body = {
  //         grant_type: 'authorization_code',
  //         code: success.code,
  //         redirect_uri: this.redirectUri,
  //         client_id: this.clientId,
  //         client_secret: this.clientSecret,
  //       };
  //       let bodyString = JSON.stringify(body); // Stringify payload
  //       let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded','Access-Control-Allow-Origin': '*', 'Access-Control-Allow-Methods': 'PUT, GET, POST, DELETE, OPTIONS' }); // ... Set content type to JSON
  //       let options = new RequestOptions({ headers: headers }); // Create a request option
  //
  //       this.http.post('https://www.linkedin.com/oauth/v2/accessToken', body, options)
  //         .map((res:Response) => res.json())
  //         .subscribe((token:any) => {
  //           this.access_token = token.access_token;
  //         });
  //       console.log("RESULT: " + JSON.stringify(success));
  //     }, error => {
  //       console.log("ERROR: ", error);
  //     });
  //     // this.navCtrl.push(Linkedin2);
  //   }
  // }
}
