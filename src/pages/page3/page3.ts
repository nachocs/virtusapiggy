import { Component, ViewChild } from '@angular/core';

import { NavController } from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { Page4 } from '../page4/page4';

@Component({
  selector: 'page-page3',
  templateUrl: 'page3.html'
})
export class Page3 {
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController) {

  }
  ngOnInit(){
  }
  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log("Current index is", currentIndex);
  }
  goBack(){
    this.navCtrl.pop();
  }
  goNext(){
    this.navCtrl.push(Page4);
  }
}
