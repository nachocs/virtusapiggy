import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Linkedin3 } from '../linkedin3/linkedin3';

@Component({
  selector: 'page-linkedin2',
  templateUrl: 'linkedin2.html'
})
export class Linkedin2 {
  items: Array<{title: string, value: string}>;
  picture_url: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.picture_url = navParams.get('pictureUrl');
    // id,picture-url,firstName,lastName,headline,public-profile-url,formatted-name
    this.items = [
      {title:'Name', value:navParams.get('formattedName')},
      {title:'Job Title', value:navParams.get('headline')},
      {title:'Time in role', value:'2 years and 7 months'},
      {title:'Salary', value:'£44,000'},
      {title:'Last Pay Rise', value:'01.08.2014'},
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Linkedin2Page');
  }
  goNext(){
    this.navCtrl.push(Linkedin3);
  }
  goBack(){
    this.navCtrl.pop();
  }
}
